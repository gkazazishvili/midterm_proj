Weather App

# The application was created to give user opportunity to see current weather by city name.

---

## Usage

* Once you open the app you will be directed to login page.
* If you don't have an account, you must go to the register page.
* After registration, user will be redirected to Home Page, where he can search city and get the weather
* User will have the right to log out.
* Application also has a 7 day weather forecast

---

## Features Used:

* Firebase
* Lottie
* Glide
* OpenWeatherMapApi