package com.example.homeworkmosh.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.homeworkmosh.model.WeatherModel
import com.example.homeworkmosh.service.WeatherAPIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainViewModel : ViewModel() {

    private val weatherApiService = WeatherAPIService()
    private val disposable = CompositeDisposable()

    val weatherData = MutableLiveData<WeatherModel>()
    val weatherError = MutableLiveData<Boolean>()
    val weatherLoading = MutableLiveData<Boolean>()

    fun refreshData(cityName: String) {
        getDataFromAPI(cityName)
    }

    private fun getDataFromAPI(cityName: String) {

        viewModelScope.launch {
            withContext(IO) {
                weatherLoading.postValue(true)
                disposable.add(
                    weatherApiService.getDataService(cityName)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(object : DisposableSingleObserver<WeatherModel>() {

                            override fun onSuccess(t: WeatherModel) {
                                weatherData.postValue(t)
                                weatherError.postValue(false)
                                weatherLoading.postValue(false)
                                Log.d("MainViewModel", "onSuccess: Success")
                            }

                            override fun onError(e: Throwable) {
                                weatherError.postValue(true)
                                weatherLoading.postValue(false)
                                Log.e("MainViewModel", "onError: $e")
                            }

                        })
                )

            }
        }
    }

}