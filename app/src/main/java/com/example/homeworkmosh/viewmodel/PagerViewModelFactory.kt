package com.example.homeworkmosh.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.homeworkmosh.service.viewPagerAPI.ApiService


class PagerViewModelFactory(private val apiService: ApiService): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(PagerViewModel::class.java)){
            return PagerViewModel(apiService) as T
        }
        throw IllegalArgumentException("not found view model")
    }

}