package com.example.homeworkmosh.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.homeworkmosh.model.Daily
import com.example.homeworkmosh.service.ApiInstance
import com.example.homeworkmosh.service.viewPagerAPI.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception


class PagerViewModel(private val api: ApiService) : ViewModel() {


    fun loadUsers() = Pager(config = PagingConfig(pageSize = 1), pagingSourceFactory = {ModelDataSource(api)}).liveData.cachedIn(viewModelScope)

    private var _dailyTemp = MutableLiveData<List<Daily>>()
    val dailyTemp: LiveData<List<Daily>>
        get() = _dailyTemp

    fun setResult() {
        viewModelScope.launch {
            val data = withContext(Dispatchers.IO) {
                ApiInstance.getDailyApi.getDaily()
            }

            try {
                if (data.isSuccessful && data.body() != null) {
                    _dailyTemp.postValue(data.body()?.daily!!)
                }
            } catch (e: Exception) {
                Log.d("error", e.toString())
            }
        }
    }
}