package com.example.homeworkmosh.viewmodel

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.homeworkmosh.model.viewPagerModel.Pager
import com.example.homeworkmosh.service.viewPagerAPI.ApiService
import okhttp3.internal.immutableListOf
import retrofit2.Response

class ModelDataSource(private val apiService: ApiService): PagingSource<Int, Pager.Data>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Pager.Data> {
        try{
            val currentPageList:Int = params.key ?: 1
            val response: Response<Pager> = apiService.getImages(currentPageList)
            val repoList:ArrayList<Pager.Data> = ArrayList<Pager.Data>()

            val data = response.body()
            if (data != null) {
                repoList.addAll(data.data!!)
            }
            val prevKey:Int? = if(currentPageList==1)
                null else currentPageList-1

            return LoadResult.Page(
                repoList,
                prevKey,
                currentPageList.plus(1)
            )
        }catch(e:Exception){
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Pager.Data>): Int? {
        return null
    }
}