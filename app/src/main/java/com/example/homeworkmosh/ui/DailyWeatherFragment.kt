package com.example.homeworkmosh.ui


import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homeworkmosh.adapter.Adapter
import com.example.homeworkmosh.adapter.LocationLoadingStateAdapter
import com.example.homeworkmosh.databinding.FragmentDailyWeatherBinding
import com.example.homeworkmosh.databinding.HandleLoadingBinding
import com.example.homeworkmosh.service.viewPagerAPI.ApiService
import com.example.homeworkmosh.viewmodel.PagerViewModel
import com.example.homeworkmosh.viewmodel.PagerViewModelFactory
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class DailyWeatherFragment : BaseFragment<FragmentDailyWeatherBinding>(
    FragmentDailyWeatherBinding::inflate
) {

    lateinit var viewMo:PagerViewModel

    lateinit var adapter: Adapter
    private var handleLoadingBinding: HandleLoadingBinding? = null

    override fun start() {

        fetchData()
        setResult()
    }

    private fun fetchData() {

        viewMo = ViewModelProvider(this, PagerViewModelFactory(ApiService.getApi()))[PagerViewModel::class.java]
        binding.rv.layoutManager = LinearLayoutManager(requireContext())
        binding.rv.itemAnimator = null
        adapter = Adapter(this)
        binding.rv.adapter = adapter


        lifecycleScope.launchWhenCreated{
            viewMo.loadUsers().observe(viewLifecycleOwner) {
                adapter.submitData(lifecycle, it)
                binding.swipe.setOnRefreshListener {
                    adapter.refresh()
                }


                lifecycleScope.launch {
                    adapter.loadStateFlow.collectLatest { loadStates ->
                        handleLoadingBinding?.progressBar?.isVisible =
                            loadStates.refresh is LoadState.Loading
                        handleLoadingBinding?.retryButton?.isVisible =
                            loadStates.refresh !is LoadState.Loading
                        handleLoadingBinding?.errorMsg?.isVisible =
                            loadStates.refresh is LoadState.Error
                    }
                }

            }
        }

    }

    private fun setResult() {
        viewMo = ViewModelProvider(this, PagerViewModelFactory(ApiService.getDailyApi()))[PagerViewModel::class.java]

        viewMo.setResult()
        viewMo.dailyTemp.observe(viewLifecycleOwner, {
            adapter.temperature = it
        })
    }

}