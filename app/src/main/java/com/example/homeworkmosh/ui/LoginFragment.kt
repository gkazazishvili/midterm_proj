package com.example.homeworkmosh.ui

import android.app.ActionBar
import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.homeworkmosh.R
import com.example.homeworkmosh.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*


class LoginFragment : BaseFragment<FragmentLoginBinding>(
    FragmentLoginBinding::inflate) {

private lateinit var firebaseAuth: FirebaseAuth
private lateinit var email:String
private lateinit var password:String

    override fun start() {
        fetch()
    }

    private fun fetch(){
        firebaseAuth = FirebaseAuth.getInstance()
        checkUser()
        listeners()

    }

    private fun checkUser() {
        val firebaseUser = firebaseAuth.currentUser

        if(firebaseUser != null){
            findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
        }
    }

    private fun listeners(){
        binding.regBtn.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        binding.loginBtn.setOnClickListener {
        validateData()
        }
    }

    private fun validateData(){
        email = binding.etEmail.text.toString()
        password = binding.etPassword.text.toString()

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            binding.etEmail.error = "Invalid email format"
            Toast.makeText(requireContext(), "Invalid email format", Toast.LENGTH_SHORT).show()
        } else if (TextUtils.isEmpty(password)){
            binding.etPassword.error = "Please enter Password"
            Toast.makeText(requireContext(), "Please enter Password", Toast.LENGTH_SHORT).show()
        } else{
            firebaseLogin()
        }
    }

    private fun firebaseLogin(){
        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener { 
                val firebaseUser = firebaseAuth.currentUser
                val email = firebaseUser!!.email
                Toast.makeText(requireContext(), "LoggedIn as $email", Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
            }
            .addOnFailureListener {
                Toast.makeText(requireContext(), "Login Failed due ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }

}