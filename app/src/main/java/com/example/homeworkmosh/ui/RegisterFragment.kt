package com.example.homeworkmosh.ui

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.homeworkmosh.R
import com.example.homeworkmosh.databinding.FragmentLoginBinding
import com.example.homeworkmosh.databinding.FragmentRegisterBinding
import com.google.android.play.core.splitinstall.d
import com.google.firebase.auth.FirebaseAuth


class RegisterFragment : BaseFragment<FragmentRegisterBinding>(
    FragmentRegisterBinding::inflate
) {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var email: String
    private lateinit var password: String

    override fun start() {
        firebaseAuth = FirebaseAuth.getInstance()
        listeners()
    }

    private fun listeners() {
        binding.signUpBtn.setOnClickListener {
            validateData()
        }
    }


    private fun validateData() {
        email = binding.etEmailRegistration.text.toString()
        password = binding.etPasswordRegistration.text.toString()

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.etEmailRegistration.error = "Invalid email format"
        } else if (TextUtils.isEmpty(password)) {
            binding.etPasswordRegistration.error = "Please enter password"
        } else if (password.length < 6) {
            binding.etPasswordRegistration.error = "Password must be at least 6 characters long"
        } else {
            firebaseSignUp()
        }
    }

    private fun firebaseSignUp() {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener {
            val firebaseUser = firebaseAuth.currentUser
            val email = firebaseUser!!.email
            Toast.makeText(
                requireContext(),
                "Account Created with email $email",
                Toast.LENGTH_SHORT
            ).show()
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }.addOnFailureListener {
            Toast.makeText(requireContext(), "Sign up failed due ${it.message}", Toast.LENGTH_SHORT)
                .show()
        }

    }
}