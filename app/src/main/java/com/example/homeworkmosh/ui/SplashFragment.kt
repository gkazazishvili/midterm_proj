package com.example.homeworkmosh.ui

import android.app.ActionBar
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.homeworkmosh.R
import com.example.homeworkmosh.databinding.FragmentSplashBinding
import com.example.homeworkmosh.viewmodel.MainViewModel
import kotlinx.coroutines.delay
import okhttp3.internal.wait

class SplashFragment : BaseFragment<FragmentSplashBinding>(
    FragmentSplashBinding::inflate){



    override fun start() {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()


            binding.lottie.animate().translationX(8000f).setStartDelay(6000).setDuration(4000)


        Handler(Looper.myLooper()!!).postDelayed({
            findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
        }, 7500)

    }



}