package com.example.homeworkmosh.ui

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.homeworkmosh.R
import com.example.homeworkmosh.databinding.FragmentHomeBinding
import com.example.homeworkmosh.viewmodel.MainViewModel
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.launch

class HomeFragment() : BaseFragment<FragmentHomeBinding>(
    FragmentHomeBinding::inflate
) {

    private lateinit var firebaseAuth: FirebaseAuth

    private val viewModel: MainViewModel by viewModels()

    private lateinit var GET: SharedPreferences
    private lateinit var SET: SharedPreferences.Editor

    override fun start() {
        checkUser()
        fetchData()
    }

    private fun checkUser() {

        firebaseAuth = FirebaseAuth.getInstance()
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser != null) {

            Toast.makeText(
                requireContext(),
                "Logged in with ${firebaseUser.email.toString()}",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            findNavController().navigate(R.id.action_homeFragment_to_loginFragment)
        }

        binding.logOutBtn.setOnClickListener {
            firebaseAuth.signOut()
            checkUser()
        }
    }

    private fun fetchData() {


        GET = requireActivity().getSharedPreferences("packageName", Context.MODE_PRIVATE)
        SET = GET.edit()


        val cName = GET.getString("cityName", "Tbilisi")
        binding.edtCityName.setText(cName)
        viewModel.refreshData(cName!!)


        getLiveData()

        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.llData.visibility = View.GONE
            binding.tvError.visibility = View.GONE
            binding.pbLoading.visibility = View.GONE

            val cityName = GET.getString("cityName", cName)
            binding.edtCityName.setText(cityName)
            viewModel.refreshData(cityName!!)
            binding.swipeRefreshLayout.isRefreshing = false
        }

        binding.imgSearchCity.setOnClickListener {
            val cityName = binding.edtCityName.text.toString()
            SET.putString("cityName", cityName)
            SET.apply()
            viewModel.refreshData(cityName)
            getLiveData()
            Log.i("TAG", "onCreate: " + cityName)
        }

    }

    private fun getLiveData() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.weatherData.observe(viewLifecycleOwner, { data ->
                data?.let {
                    binding.llData.visibility = View.VISIBLE

                    binding.tvCityCode.text = data.sys?.country.toString()
                    binding.tvCityName.text = data.name.toString()

                    Glide.with(requireContext())
                        .load(
                            "https://openweathermap.org/img/wn/" + (data.weather?.get(0)?.icon
                                ?: "") + "@2x.png"
                        )
                        .into(img_weather_pictures)


                    tv_degree.text = data.main?.temp.toString() + "°C"

                    tv_humidity.text = data.main?.humidity.toString() + "%"
                    tv_wind_speed.text = data.wind?.speed.toString()
                    tv_lat.text = data.coord?.lat.toString()
                    tv_lon.text = data.coord?.lon.toString()

                }
            })

            viewModel.weatherError.observe(viewLifecycleOwner, { error ->
                error?.let {
                    if (error) {
                        tv_error.visibility = View.VISIBLE
                        pb_loading.visibility = View.GONE
                        ll_data.visibility = View.GONE
                    } else {
                        tv_error.visibility = View.GONE
                    }
                }
            })

            viewModel.weatherLoading.observe(viewLifecycleOwner, { loading ->
                loading?.let {
                    if (loading) {
                        pb_loading.visibility = View.VISIBLE
                        tv_error.visibility = View.GONE
                        ll_data.visibility = View.GONE
                    } else {
                        pb_loading.visibility = View.GONE
                    }
                }
            })

        }
    }
}