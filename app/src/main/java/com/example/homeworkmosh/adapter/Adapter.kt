package com.example.homeworkmosh.adapter

import android.annotation.SuppressLint
import android.content.ContentProvider
import android.icu.text.Transliterator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.animation.content.Content
import com.bumptech.glide.Glide
import com.example.homeworkmosh.databinding.ItemBinding
import com.example.homeworkmosh.model.Daily
import com.example.homeworkmosh.model.viewPagerModel.Pager
import com.example.homeworkmosh.ui.DailyWeatherFragment
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.google.android.play.core.splitinstall.d
import com.google.firebase.auth.ktx.oAuthProvider
import kotlinx.android.synthetic.main.fragment_home.*


class Adapter(val context:DailyWeatherFragment): PagingDataAdapter<Pager.Data, Adapter.ViewHolder>(DataDiff) {

    var temperature: List<Daily> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    object DataDiff : DiffUtil.ItemCallback<Pager.Data>(){
        override fun areItemsTheSame(oldItem: Pager.Data, newItem: Pager.Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pager.Data, newItem: Pager.Data): Boolean {
            return oldItem == newItem
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    inner class ViewHolder(private val binding:ItemBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var currentData: Daily

        @SuppressLint("SetTextI18n")
        fun onBind(){


            currentData = temperature[absoluteAdapterPosition]
            Glide.with(itemView)
                .load(
                    "https://openweathermap.org/img/wn/" + (currentData.weather?.get(0)?.icon
                        ?: "") + "@2x.png"
                )
                .into(binding.imgWeatherPictures)
            binding.dayAverage.text = currentData.temp?.day.toString() + "°" // 08
            binding.dayMax.text = currentData.temp?.max.toString() + "°" // 09
            binding.dayMin.text = currentData.temp?.min.toString() + "°" // 09



        }

    }

    override fun getItemCount() = temperature.size


}