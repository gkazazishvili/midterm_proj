package com.example.homeworkmosh.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

import androidx.recyclerview.widget.RecyclerView
import com.example.homeworkmosh.R
import com.example.homeworkmosh.databinding.HandleLoadingBinding

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest

class LocationLoadingStateAdapter(
    private val adapter: Adapter
) : LoadStateAdapter<LocationLoadingStateAdapter.NetworkStateItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState) =
        NetworkStateItemViewHolder(
            HandleLoadingBinding.bind(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.handle_loading, parent, false)
            )
        ) {
            adapter.retry()
        }

    override fun onBindViewHolder(holder: NetworkStateItemViewHolder, loadState: LoadState) =
        holder.bind(loadState, adapter)




    class NetworkStateItemViewHolder(
        private val binding: HandleLoadingBinding,
        private val retryCallback: () -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.retryButton.setOnClickListener {
                retryCallback()
            }
        }

        fun bind(loadState: LoadState, adapter: Adapter) {

                binding.progressBar.isVisible = loadState is LoadState.Loading
                binding.retryButton.isVisible = loadState is LoadState.Error
                binding.errorMsg.isVisible =
                    !(loadState as? LoadState.Error)?.error?.message.isNullOrBlank()
                binding.errorMsg.text = (loadState as? LoadState.Error)?.error?.message


            }
        }
    }