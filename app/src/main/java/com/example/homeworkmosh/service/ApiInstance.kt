package com.example.homeworkmosh.service

import com.example.homeworkmosh.service.viewPagerAPI.ApiService
import com.example.homeworkmosh.service.viewPagerAPI.Constant
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiInstance {

    val getDailyApi: WeatherAPI =
             Retrofit.Builder()
            .baseUrl(Constant.DAILY_URL)
            .addConverterFactory(MoshiConverterFactory.create(
                Moshi.Builder().addLast(
                    KotlinJsonAdapterFactory()
                ).build()))
            .build()
            .create(WeatherAPI::class.java)
    }
