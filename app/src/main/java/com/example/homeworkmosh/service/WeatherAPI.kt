package com.example.homeworkmosh.service

import com.example.homeworkmosh.model.Day
import com.example.homeworkmosh.model.WeatherModel
import com.example.homeworkmosh.model.viewPagerModel.Pager
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface WeatherAPI {

    //http://api.openweathermap.org/data/2.5/weather?q=bingöl&APPID=04a42b96398abc8e4183798ed22f9485

    @GET("data/2.5/weather?&units=metric&APPID=04a42b96398abc8e4183798ed22f9485")
    fun getData(
        @Query("q") cityName: String
    ): Single<WeatherModel>


    @GET("/data/2.5/onecall?lat=41&lon=44&exclude=current,minutely,hourly,alerts&appid=6a5c61db40d3d90f0f823f8c86307dc0&units=metric")
    suspend fun getDaily(): Response<Day>

}