package com.example.homeworkmosh.service.viewPagerAPI

import android.provider.SyncStateContract
import com.example.homeworkmosh.model.viewPagerModel.Pager
import com.example.homeworkmosh.service.viewPagerAPI.Constant.Companion.DAILY_URL
import com.example.homeworkmosh.service.viewPagerAPI.Constant.Companion.PAGER_URL
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit

import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {


    @GET("users")
    suspend fun getImages(@Query("page") page: Int): Response<Pager>


    companion object {

        fun ok(): OkHttpClient {
            val builder = OkHttpClient().newBuilder()

            builder.addInterceptor(Interceptor {
                val request = it.request()
                it.proceed(request)
            })
            return builder.build()
        }

        private val moshi2 = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()

        fun getApi() = Retrofit.Builder()
            .baseUrl(PAGER_URL)
            .client(ok())
            .addConverterFactory(MoshiConverterFactory.create(moshi2))
            .build().create(ApiService::class.java)

        fun getDailyApi() =
            Retrofit.Builder()
                .baseUrl(DAILY_URL)
                .addConverterFactory(MoshiConverterFactory.create(moshi2))
                .build()
                .create(ApiService::class.java)
    }


}


