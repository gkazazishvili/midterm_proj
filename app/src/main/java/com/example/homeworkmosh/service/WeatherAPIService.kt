package com.example.homeworkmosh.service

import com.example.homeworkmosh.model.WeatherModel
import com.squareup.moshi.Moshi

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class WeatherAPIService {
    private val BASE_URL = "http://api.openweathermap.org/"


    private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(WeatherAPI::class.java)




    fun getDataService(cityName: String): Single<WeatherModel> {
        return api.getData(cityName)
    }

}