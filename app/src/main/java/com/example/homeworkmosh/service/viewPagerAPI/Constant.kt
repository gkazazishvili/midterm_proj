package com.example.homeworkmosh.service.viewPagerAPI

import okhttp3.OkHttpClient

class Constant {
    companion object{
        const val PAGER_URL = "https://reqres.in/api/"
        const val DAILY_URL = "https://api.openweathermap.org/"

    }

}